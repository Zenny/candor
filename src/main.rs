#![windows_subsystem = "windows"]

mod crypt;


extern crate web_view;
use web_view::*;

#[macro_use] extern crate serde_json;
use serde_json::{Value, Error};

use std::env;

use std::string::String;

use std::path::Path;
use std::fs::File;
use std::io::Write;
use std::fs;

// Taken from: https://stackoverflow.com/questions/47070876/how-can-i-merge-two-json-objects-with-rust
// Credit: Shepmaster
fn merge_json(a: &mut Value, b: Value) {
    match (a, b) {
        (a @ &mut Value::Object(_), Value::Object(b)) => {
            let a = a.as_object_mut().unwrap();
            for (k, v) in b {
                merge_json(a.entry(k).or_insert(Value::Null), v);
            }
        }
        (a, b) => *a = b,
    }
}

fn split<'a>(s: &'a str, pat: &str) -> Vec<&'a str> {
    return s.split(pat).collect();
}

fn slice_path(path: &str) -> &str {
    let sp: Vec<&str> = path.splitn(8, '/').collect();
    return sp[7];
}

fn dir(path: &str) -> String {
    return format!("{}{}", env::current_dir().unwrap().display(), path);
}

fn cdfile(path: &str) -> String
{
    return format!("file:///{}/view/{}", env::current_dir().unwrap().display(), path);
}

fn error_dlg(view: &mut WebView<Value>, e: &str) {
    view.dialog( Dialog::Alert(Alert::Error), "Error!", e );
}

fn init_view(view: &mut WebView<Value>) {
    println!("Init go!");
    view.set_color(16, 16, 16, 255);

}

///
/// Called when an external call is received from the web view.
///
//noinspection RsAssignToImmutable
#[allow(unused_must_use)]
fn invoke(view: &mut WebView<Value>, arg: &str, userdata: &mut Value) {
    //println!("{}",arg);
    //println!("data: {}", userdata.to_string());
    match arg {
        "CA~appload" => {
            if !Path::new("./usr/store.dat").exists() || // File doesn't exist
                fs::metadata("./usr/store.dat").unwrap().len() == 0 { // File is empty
                let mut usrdir = Ok(());
                if !Path::new("./usr").exists() {
                    usrdir = fs::create_dir("./usr");
                }

                if usrdir.is_ok() {
                    let store = File::create("./usr/store.dat");

                    if store.is_ok() { // We just created the store file, time to do some setup
                        view.eval("hideLoad()");
                        // Setup completes at: CA~pk
                    }
                    else {
                        error_dlg(view, &format!("Error: CA1001\n{:?}",store.unwrap_err()));
                    }
                }
                else {
                    error_dlg(view, &format!("Error: CA1002\n{:?}",usrdir.unwrap_err()));
                }

            }
            else { // Our file is already setup, that means we prompt them instead to decrypt it.
                userdata["state"] = json!(1); // IGNORE ERR
                view.eval(r#"
                    setupToIntro();
                    waitHideLoad(255);
                "#);
            }
        },
        "CA~guest" => {
            view.eval(r#"
                $('#pk').val('');
                showID('load-wrap');
                waitHideDelID('setup-wrap', 255);
                enableTabs("main");
            "#);
            if userdata["state"] == 0 && (Path::new("./usr/store.dat").exists() || // File exists
               fs::metadata("./usr/store.dat").unwrap().len() == 0) { // File is empty
                fs::remove_dir_all("./usr");
            }
            view.eval("waitHideLoad(255)");
        },
        _ => {
            if arg.starts_with("CA~pk:") { // They entered a private key for some reason.
                let (_, pkbytes) = arg.split_at(6);
                if userdata["state"] == 0 { // Creating store.dat file for first time
                    view.eval(r#"
                        showID('load-wrap');
                        waitHideDelID('setup-wrap', 255);
                    "#);
                    let data = json!({
                        "setup": true
                    }).to_string();
                    let enc = crypt::aes256_enc(&data.as_bytes(), pkbytes.as_bytes(),
                                                &[1,2,3]);
                    let mut store = File::create("./usr/store.dat").unwrap();
                    store.write(&enc);
                    view.eval("waitHideLoad(255)");
                }
                else if userdata["state"] == 1 { // Returning, decrypt store.dat
                    let store = fs::read("./usr/store.dat").unwrap();
                    let data = crypt::aes256_dec(&store, pkbytes.as_bytes(), &[1,2,3]);
                    if data.is_some() {
                        view.eval(r#"
                            showID('load-wrap');
                            waitHideDelID('setup-wrap', 255);
                        "#);

                        let json: Value = serde_json::from_str(&String::from_utf8_lossy(
                                                &data.unwrap()
                                            )).unwrap(); // Get the json data from the file

                        merge_json(userdata, json); // Put it in our session
                        view.eval("waitHideLoad(255)");
                    }
                    else {
                        view.eval("pkErr()");
                    }
                }
            }
            else {
                println!("Unknown invoke: {}", arg);
            }
        }
    }
}


fn main() {
    let size = (1280, 720);
    let resizable = true;
    let debug = true;
    let userdata: Value = json!({
                    "state": 0
                  });

    web_view::run("Candor", Content::Url(cdfile("index.html")), Some(size), resizable, debug,
                  |mut _view|{
                      _view.dispatch(|mut _v, _| init_view(&mut _v));
                  },
                  invoke, userdata);

}
