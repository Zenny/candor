'use strict';

let i = 0;
let isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
let debug = true;

function change()
{
    document.getElementById("btn1").innerText = "Test "+ window.location ;
}

function load(file)
{
    window.location.replace(file);
}

function pkErr()
{
    $("#pk").addClass("txtbx_err");
}

function sendKey()
{
    let pk = $("#pk").val();
    if(pk !== "")
    {
        window.external.invoke('CA~pk:'+pk);
        $("#pk").val("");
        enableTabs("main");
    }
    else
    {
        pkErr();
    }
}

function hideLoad() // temp
{
    setTimeout(function(){ // Artificial for now, lets the transition set up.
        hideID('load-wrap')
    }, 150);
}

function waitHideLoad(i) // temp
{
    setTimeout(function(){ // Artificial for now, lets the transition set up.
        hideID('load-wrap')
    }, i);
}

function hideID(id)
{
    let el = $("#"+id);
    el.addClass("invis");
    setTimeout(function(){
        el.css("height","0");
    }, 255); // After the invis transition
}

function hideDelID(id)
{
    let el = $("#"+id);
    el.addClass("invis");
    setTimeout(function(){
        el.remove();
    }, 255); // After the invis transition
}

function waitHideDelID(id, wait)
{
    setTimeout(function(){
        let el = $("#"+id);
        el.addClass("invis");
        setTimeout(function(){
            el.remove();
        }, 255); // After the invis transition
    }, wait); // After the invis transition
}

function showID(id)
{
    let el = $("#"+id);
    el.css("height","");
    el.removeClass("invis");
}

function del(id)
{
    $("#"+id).remove();
}

function pkFocus()
{
    $("#pk").removeClass("txtbx_err");
}

function disableTabs(id)
{ //foreach child set data-tab to tabindex and tabindex to -1
    $("#"+id).children().attr("tabindex", "-1");
}

function enableTabs(id)
{
    $("#"+id).children().removeAttr("tabindex");
}


function pkCount(e)
{
    let len = $("#pk").val().length;
    $("#pk").attr("title", len + " characters");
    if(len > 0)
        $("#pksend").removeAttr("disabled");
    else
        $("#pksend").attr("disabled", "");

    if(e.keyCode == 13) { // ENTER
        sendKey();
    }
}

// Turns the setup divs into a "welcome back"
function setupToIntro()
{
    $("#setuptitle").text("Welcome Back!");
    $("#intro").text(
    "Welcome back to Candor. A decentralized chat application where power and security is in your hands. Enter your password below so we can decrypt your local data."
    );
}

$(function(){
    if(isChrome && debug)
        hideLoad();
    disableTabs("main");
    let pk = $("#pk");
    pk.focus(pkFocus);
    pk.keyup(pkCount);
    $("#pksend").click(sendKey);
    window.external.invoke('CA~appload');
});